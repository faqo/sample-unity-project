﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformManager : MonoBehaviour {

	public int maxPlatforms = 20;
	public GameObject platformPrefab;
	public float horizontalMin = 6.5F;
	public float horizontalMax = 14F;
	public float verticalMin = -6F;
	public float verticalMax = 6F;

	private Vector2 originPosition;

	void Start () {
		originPosition = transform.position;	
		Spawn();
	}
	
	void Spawn () {

		for(int i = 0; i < maxPlatforms; i++){
			float y = (Random.value > 0.5F ? -1F : 1F) * Random.Range(verticalMin, verticalMax);
			Vector2 randomPos = originPosition + new Vector2(Random.Range(horizontalMin, horizontalMax), y);

			Instantiate(platformPrefab, randomPos, Quaternion.identity);
			originPosition = randomPos;
		}
	}
}
