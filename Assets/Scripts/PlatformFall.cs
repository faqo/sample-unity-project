﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFall : MonoBehaviour {

	public float fallDelay = 1F;

	private Rigidbody2D body;
	

	void Start () {
		body = GetComponent<Rigidbody2D>();	
	}


	void OnCollisionEnter2D(Collision2D other){
		if(other.gameObject.CompareTag("Player"))
			Invoke("Fall", fallDelay);
	}

	private void Fall(){
		body.isKinematic = false;
	}
}
