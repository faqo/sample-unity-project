﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePlatformController : MonoBehaviour {

	[HideInInspector]
	public bool isFacingRight = true;

	[HideInInspector]
	public bool isJump = false;

	public float moveForce = 365F;
	public float maxSpeed = 5F;
	public float jumpForce = 1000F;
	public Transform groundCheck;

	private bool isGrounded = false;
	private Animator animator;
	private Rigidbody2D body;

	void Start () {
		animator = GetComponent<Animator>();
		body = GetComponent<Rigidbody2D>();
		
	}
	
	void Update () {
		isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		if(Input.GetButtonDown("Jump") && isGrounded)
			isJump = true;
	}

	void FixedUpdate(){
		float h = Input.GetAxis("Horizontal");
		animator.SetFloat("Speed", Mathf.Abs(h));

		if(h * body.velocity.x < maxSpeed)
			body.AddForce(Vector2.right * h * moveForce);

		if(Mathf.Abs(body.velocity.x) > maxSpeed)
			body.velocity = new Vector2(Mathf.Sign(body.velocity.x) * maxSpeed, body.velocity.y);

		if((h > 0 && !isFacingRight) || (h < 0 && isFacingRight))
			Flip();

		if(isJump){
			animator.SetTrigger("Jump");
			body.AddForce(Vector2.up * jumpForce);
			isJump = false;
		}

	}

	void Flip(){
		isFacingRight = !isFacingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
